from django.urls import path
from . import views

urlpatterns = [
    path('', views.usercontacts, name='usercontacts'),
    path('add_person', views.add_person, name='add_person'),
]
