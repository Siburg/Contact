from django.shortcuts import render
#from django.http import HttpResponse

# Create your views here.
def index(request):
    """View for the home page of the project, i.e. the index page for its web location"""
    return render(request, 'index.html')

def usercontacts(request):
    """View for """
    return render(request, 'usercontacts.html')

def add_person(request):
    """View for """
    return render(request, 'add_person.html')
