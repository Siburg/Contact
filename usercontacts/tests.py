from django.test import TestCase
from usercontacts.models import Person


# Create your tests here.
class ModelTests(TestCase):
    """tests of the models"""

    def test_person_str(self):
        alice = Person(first_name='Alice', last_name='Avery')
        self.assertEquals(str(alice),'Alice Avery')