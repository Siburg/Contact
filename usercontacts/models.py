from django.db import models

# Create your models here.

class Person (models.Model):
    first_name = models.CharField(
        max_length=25,
        blank=True,
        default='',
        db_index=True,
    )
    last_name = models.CharField(
        max_length=50,
        blank=True,
        default='',
        db_index=True,
    )
    prefix = models.CharField(
        max_length=20,
        blank=True,
        default='',
    )
    initials = models.CharField(
        max_length=8,
        blank=True,
        default='',)
    email = models.EmailField(
        blank=True,
        default = '',
    )
    mobile = models.CharField(
        max_length=20,
        blank=True,
        default='',
    )
    date_of_birth = models.DateField(
        null=True,
        blank=True,
    )
    notes = models.TextField(
        blank=True,
        default = '',
    )

# do something to ensure that first_name and last_name cannot both be NULL

    def __str__(self):
        return self.first_name + ' ' + self.last_name

