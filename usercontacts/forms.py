from django import forms
from . import Person

first_name = forms.CharField(
    max_length=25,
)
last_name = forms.CharField(
    max_length=50,
)
prefix = forms.CharField(
    max_length=20,
)
initials = forms.CharField(
    max_length=8,
)
email = forms.EmailField(
)
mobile = forms.CharField(
    max_length=20,
)
date_of_birth = forms.DateField(
)
notes = forms.TextField(
)


class PersonForm(forms.ModelForm):

    class Meta:
        model = Person
        