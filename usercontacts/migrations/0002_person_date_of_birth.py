# Generated by Django 2.0.1 on 2018-01-28 00:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('usercontacts', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='date_of_birth',
            field=models.DateField(blank=True, null=True),
        ),
    ]
