# Generated by Django 2.0.1 on 2018-01-27 02:54

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=25)),
                ('last_name', models.CharField(max_length=50)),
                ('prefix', models.CharField(max_length=20)),
                ('initials', models.CharField(max_length=8)),
                ('mobile', models.CharField(max_length=20)),
            ],
        ),
    ]
